﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPL_APP
{
    /// <summary>
    /// Iterator for MethodCollection
    /// </summary>
    public class Iterator : IAbstractIterator
    {
        private MethodCollection _collection;
        private int _current = 0;
        private int _step = 1;

        // Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="collection"></param>
        public Iterator(MethodCollection collection)
        {
            this._collection = collection;
        }

        // Gets first item
        /// <summary>
        /// Gets the first element
        /// </summary>
        /// <returns></returns>
        public Methods First()
        {
            _current = 0;
            return _collection[_current] as Methods;
        }

        // Gets next item
        /// <summary>
        /// Gets the next element
        /// </summary>
        /// <returns></returns>
        public Methods Next()
        {
            _current += _step;
            if (!IsDone)
                return _collection[_current] as Methods;
            else

                return null;
        }

        // Gets or sets stepsize
        /// <summary>
        /// Steps to take
        /// </summary>
        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }

        // Gets current iterator item

        public Methods CurrentMethods
        {
            get { return _collection[_current] as Methods; }
        }

        // Gets whether iteration is complete

        public bool IsDone
        {
            get { return _current >= _collection.Count; }
        }
    }
}
