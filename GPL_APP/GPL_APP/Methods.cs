﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPL_APP
{   
    /// <summary>
    /// Stores all the methods 
    /// </summary>
    public class Methods
    {
        /// <summary>
        /// Name of the method
        /// </summary>
        public string methodName { get; set; }
        /// <summary>
        /// Parameters to save from methods
        /// </summary>
        public string[] parameters { get; set; }
        /// <summary>
        /// Content of the method
        /// </summary>
        public ArrayList content;

        /// <summary>
        /// Constructor initializes arraylist where content is stored.
        /// </summary>
        public Methods()
        {
            content = new ArrayList();
        }
        /// <summary>
        /// Content of the method is stored here.
        /// </summary>
        /// <param name="line"></param>
        public void AddContent(string line)
        {
            content.Add(line);
        }
        /// <summary>
        /// Clears all the content inside the method.
        /// </summary>
        public void Clear()
        {
            methodName= "";
            parameters = null;
            content.Clear();
        }
    }
}
