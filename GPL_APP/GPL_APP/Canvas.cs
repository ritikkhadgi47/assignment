﻿using System;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace GPL_APP
{
    /// <summary>
    /// Stores graphics, pen, cursor position and performs clearing and reseting.
    /// </summary>
    public class Canvas
    {
        /// <summary>
        /// boolean value for flag and running
        /// </summary>
        bool flag = false, running = false;
        /// <summary>
        /// Threading for flashing Colors
        /// </summary>
        Thread flashThread;
        /// <summary>
        /// Delegate for providing reference to the picture box.
        /// </summary>
        
        public delegate void RefereshDisplayDelegate();
        /// <summary>
        /// Delegate for providing reference to the run button.
        /// </summary>
        /// <param name="value"></param>
        public delegate void RunBtnDelegate(bool value);
        /// <summary>
        /// Delegate for providing reference to the textarea 
        /// </summary>
        /// <param name="text"></param>
        public delegate void TextAreaDelegate(StringBuilder text);
        PictureBox display;
        Button runBtn;
        TextBox errorBox;
        //instance data for pen and x,y pos and graphics context
        /// <summary>
        /// Graphics for drawing shapes and lines
        /// </summary>
        public Graphics g;

        /// <summary>
        /// Brush for fill up the shapes
        /// </summary>
        public SolidBrush Brush;
        /// <summary>
        /// Position where shapes are drawn from
        /// </summary>
        public int xPos, yPos;
        /// <summary>
        /// Colors for individual shapes
        /// </summary>
        public Color colour;

        /// <summary>
        /// Fill or no Fill
        /// </summary>
        public bool fill = false;
        /// <summary>
        /// For running the program
        /// </summary>
        public bool run = false;

        //contructor initializes canvas to white and pen in black at 0,0

        /// <summary>
        /// Initializes all the instance variable
        /// </summary>
        /// <param name="g">Graphic object of a bitmap where shapes are drawn.</param>
        public Canvas(Graphics g, PictureBox display)
        {
            
            this.g = g; //for main 
            xPos = yPos = 100;
            colour = Color.Black;
            Brush = new SolidBrush(colour);
            this.display = display;
        }

        /**
         * Setting up delegates
         */

        public void InvokeDelegate()
        {
            try
            {
                display.Invoke(new RefereshDisplayDelegate(RefereshDisplay));
            }
            catch (InvalidOperationException)
            {

            }
            
        }
        /// <summary>
        /// Invokes the console textarea.
        /// </summary>
        /// <param name="text"></param>
        public void InvokeConsoleDelegate(StringBuilder text)
        {
            try
            {
                runBtn.Invoke(new TextAreaDelegate(WriteErrors), text);
            }
            catch (InvalidOperationException)
            {

            }

        }
        /// <summary>
        /// Invokes the Run button
        /// </summary>
        /// <param name="value"></param>
        public void InvokeBtnDelegate(bool value)
        {
            try
            {
                runBtn.Invoke(new RunBtnDelegate(BtnStatus), value);
            }
            catch (InvalidOperationException)
            {

            }

        }
        public void RefereshDisplay()
        {
            display.Invalidate();
        }
        public void WriteErrors(StringBuilder errors)
        {
            this.errorBox.Text = errors.ToString();
        }
        public void BtnStatus(bool value)
        {
            runBtn.Enabled = value;
        }

        public void SetRunBtn(Button btn)
        {
            this.runBtn = btn;
        }
        public void SetTextBox(TextBox textbox)
        {
            this.errorBox = textbox;
        }


        /**
         * Setting up threads and Delegates Completed
         */


        /// <summary>
        /// 
        /// </summary>
        /// <param name="colorName"></param>
        public void SetBrushAndPenColor(String colorName)
        {
            Console.WriteLine("hello");
            
            this.colour = Color.FromName(colorName);
            Console.WriteLine(colour);
            Console.WriteLine(colorName);
            /*if (colorName.Equals("redgreen"))
                 {
                     Console.WriteLine("hello");
                     string colour = "redgreen";
                     flashThread = new System.Threading.Thread(flashing);
                     flashThread.Start(colour);


                 }else if(colour.Equals("blueyellow"))
                 {
                     string colour = "blueyellow";
                     flashThread = new System.Threading.Thread(flashing);
                     flashThread.Start(colour);

                 }
                 else if (colour.Equals("bluewhite"))
                 {
                     string colour = "blueyellow";
                     flashThread = new System.Threading.Thread(flashing);
                     flashThread.Start(colour);

                 }
                 else
                 {*/
            Brush.Color = colour;
                Console.WriteLine(colour);
                Console.WriteLine("heelo");
         
            
        }

        /// <summary>
        /// method to start threading
        /// </summary>
        /// <param name="str"></param>
        /*public void flashing(object str)
        {
            if (str.Equals("redgreen"))
            {

                while (true)
                {
                    
                    while (Brush.Color!=Color.Yellow)
                    {
                        //Console.WriteLine("hello");
                        Console.WriteLine(flag);
                       
                        if (flag == false)
                        {
                            Console.WriteLine("HElloooo");
                            Brush.Color = Color.Red;
                            Console.WriteLine(Brush.Color);
                            flag = true;
                        }
                        else
                        {
                            Brush.Color = Color.Green;
                            flag = false;

                        }
                        Thread.Sleep(500);
                    }
                }
            }else if (str.Equals("blueyellow"))
            {
                while (true)
                {
                    while (running == true)
                    {
                        if (flag == false)
                        {
                            Brush.Color = Color.Blue;
                            flag = true;
                        }
                        else
                        {
                            Brush.Color = Color.Yellow;
                            flag = true;

                        }
                        Thread.Sleep(500);
                    }
                }
            }
            else
            {
                while (true)
                {
                    while (running == true)
                    {
                        if (flag == false)
                        {
                            Brush.Color = Color.Blue;
                            flag = true;
                        }
                        else
                        {
                            Brush.Color = Color.White;
                            flag = true;

                        }
                        Thread.Sleep(500);
                    }
                }
            }
        }*/

        

        /// <summary>
        /// Draw a straight line
        /// </summary>
        /// <param name="xy">Position where line needs to be drawn to</param>
        public void DrawLine(int[] xy)
        {

            Console.WriteLine("Drawline Responded from the Canvas!" + xPos + " " + yPos + " " + xy[0] + " " + xy[1]);
            //draw a line from current (xPos, yPos) to provided arguments (toX, toY)
            g.DrawLine(new Pen(colour), xPos, yPos, xy[0], xy[1]);//draw line
            xPos = xy[0];
            yPos = xy[1];
           


        }
        /// <summary>
        /// Changes the location of the cursor
        /// </summary>
        /// <param name="xy">Position where cursor needs to be moved.</param>
        public void ChangeCursorLocation(int[] xy)
        {
            xPos = xy[0];
            yPos = xy[1];
        }

        /// <summary>
        /// Clears all the drawing in the bitmap.
        /// </summary>
        public void Clear()
        {
            g.Clear(Color.Transparent);
            
        }

        /// <summary>
        /// Resets all the textbox as well as colours, cursor, and errors.
        /// </summary>
        public void Reset()
        {
            Console.WriteLine("Hello");
            SolidBrush brush = new SolidBrush(Color.Green);
            xPos = yPos = 100;
            fill = false;
            colour = Color.Black;
            g.FillEllipse(brush, xPos - 2, yPos - 2, 2 * 2, 2 * 2);
        }

        /// <summary>
        /// Draws the cursor in the bitmap
        /// </summary>
        /// <param name="g"></param>
        public void DrawCursor(Graphics g)
        {
            SolidBrush brush = new SolidBrush(Color.Green);


            g.Clear(Color.Transparent);
            g.FillEllipse(brush, xPos, yPos, 2 * 2, 2 * 2);
        }


    }
}
