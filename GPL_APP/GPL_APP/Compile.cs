﻿using System;
using System.Collections;

namespace GPL_APP
{   
    /// <summary>
    /// Compiles the command before it is parsed in the parser.
    /// </summary>
    public class Compile
    {
        public ArrayList errors;

        /// <summary>
        /// Initialized intance variable errors to an arraylist.
        /// </summary>
        public Compile()
        {
            errors = new ArrayList();
        }

        /// <summary>
        /// Trims and Lowers the given string
        /// </summary>
        /// <param name="line">String to be lowered and trimed.</param>
        /// <returns></returns>
        public String TrimLower(String line)
        {
            String parse = line.ToLower().Trim();
            return parse;
        }
        
        /// <summary>
        /// Parses the line into cmd and parameter.
        /// </summary>
        /// <param name="line">String as a command.</param>
        /// <returns></returns>
        public String[] LineParser(String line)
        {
            //accepts a line and returns array of two words seperated by space
            String LineParse = line.Trim();
            String[] split = LineParse.Split(' ');
            return split;
        }
        /// <summary>
        /// Parses the multiline command into single line command.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public String[] TextParser(String text)
        {
            //accepts a text and returns array of line strings.
            String TextParse = text.Trim();
            String[] split = TextParse.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            return split;
        }

        /// <summary>
        /// count and track the errors
        /// </summary>
        public ArrayList getErrors()
        {
            return errors;
        }

        public int getErrorCount()
        {
            return errors.Count;
        }
        public void ClearErrors()
        {
            errors.Clear();
        }
    }
}
