﻿using System;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace GPL_APP
{
    /// <summary>
    /// Checks syntax and stores variables and methods.
    /// </summary>
    public class Syntax : Compile
    {
        /// <summary>
        /// Variables are stores here.
        /// </summary>
        public Hashtable variables;
        /// <summary>
        /// Methods are stored here.
        /// </summary>
        public MethodCollection methods;
        /// <summary>
        /// Referece for itereator object.
        /// </summary>
        Iterator iterator;
        /// <summary>
        /// Reference for Datatable object.
        /// </summary>
        DataTable dt;

        /// <summary>
        /// Constructor for initializing vairables methods and datatable.
        /// </summary>
        public Syntax()
        {
            dt = new DataTable();
            variables = new Hashtable();
            methods = new MethodCollection();
        }

        /// <summary>
        /// Find the given word in given line.
        /// </summary>
        /// <param name="line">Line which is to be check</param>
        /// <param name="word">Word which is to be found.</param>
        /// <returns></returns>
        public bool FindWord(string line, string word)
        {
            Match m = Regex.Match(line, @"\b" + word + @"\b");
            if (m.Success)
            {
                return true;
            }

            return false;

        }

        /*
         * VARIABLE PARSING STARTS
         */

        /// <summary>
        /// If a line contains any variable declaration or variable reassignment this method is called.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool VarSyntaxCheck(String text)
        {
            

            Console.WriteLine("****Inside VarSyntaxCheck****");
            int value;
            string[] tokens = Array.ConvertAll(text.ToLower().Trim().Split('='), s => s);
            tokens[0] = tokens[0].Trim();

            if (tokens.Length == 2)
            {


                //if variable is already contained overrride it
                if (variables.ContainsKey(tokens[0]) && int.TryParse(tokens[1], out value))
                {
                    int.Parse(tokens[1]);
                    Console.WriteLine(variables[tokens[0]]);

                    variables[tokens[0]] = value;

                    Console.WriteLine(variables[tokens[0]]);

                    return true;


                }
                //if varname exist but assigned value is not parsable
                else if (variables.ContainsKey(tokens[0]) && !int.TryParse(tokens[1], out value))
                {

                    //replacing all the variables found in second part of assignment
                    tokens[1] = VarToValue(tokens[1]);
                    Console.WriteLine("Token store after = sign parsed if var is found with result: " + tokens[1]);

                    if (int.TryParse(string.Format("{0}", dt.Compute(tokens[1], "")), out value))
                    {
                        variables[tokens[0]] = value;
                        Console.WriteLine(tokens[1] + " computed with return value " + value + " new value of variable is " + variables[tokens[0]].ToString());

                        return true;


                    }

                }
                //if variable is not contained  store it
                else
                {
                    Match m = Regex.Match(tokens[0], @"\b[a-zA-Z_]+[0-9]*\b");
                    int.Parse(tokens[1]);
                    if (m.Success && int.TryParse(tokens[1], out value))
                    {
                        variables.Add(tokens[0], value);
                        Console.WriteLine(tokens[0] + "added with value" + value);
                        return true;

                    }
                }

            }
        return false;
            
        }

        /// <summary>
        /// Check if the line contains any vairable stores and parse it for the value of the variable.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public string VarToValue(string line)
        {
            ICollection key = variables.Keys;

            Console.WriteLine(variables.Count);
            foreach (string k in key)
            {
                Console.WriteLine(k + "checking variables inside vartovalue");

                Match m = Regex.Match(line, @"\b" + k + @"\b");
                if (m.Success)
                {
                    Console.WriteLine(line + " ***this line is parsed for value***" + variables[k].ToString());
                    line = Regex.Replace(line, @"\b" + k + @"\b", variables[k].ToString());

                }

            }
            return line;

        }

        /**
         * VARIABLE PARSING ENDS
         */

        /**
         * IF AND WHILE PARSING STARTS
         */

        
        
        public bool CheckCondition(string line)
        {
            /***
             * Needs to check condition not just by spliting with ' ' as conditions can have multiple ' 's
             */
            bool result = false;
            Console.WriteLine("checking condition for line: " + line);
            string[] splitLine = line.Split(' ');
            Console.WriteLine("*****" + splitLine[1] + "******");
            string condition = VarToValue(splitLine[1]);
            Console.WriteLine("*****"+ condition + "******");

            DataTable dt = new DataTable();
            try
            {
                result= bool.Parse(string.Format("{0}", dt.Compute(condition, "")));
            }
            catch (Exception)
            {
                throw new InvalidCastException();
            }
            return result;
        }
        /// <summary>
        /// Save the offset of all the if and while found.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        public Hashtable WhileIfSyntaxCheck(string[] cmd, string[] endpoint)
        {
            Console.WriteLine("****Inside WhileIfSyntaxCheck****");
            //tempoffset is kept one index higher then it should be because tempoffset is also used
            //as a flag for knowing that if is found if tempoffet > 0 so in case if is at the top tempoffset
            //whill still be set to zero and the algorithm thinks that if is not found.
            int tempoffset = 0; //store temp offset of if
            Hashtable arranged = new Hashtable(); //store arraged offset of all if's and endif's
            int count = 0; //count the number of if's after an if
            bool ifreturn = false;
            //get all if's and endif
            for (int j = 0; j < cmd.Length; j++)
            {

                //no if's store the offset of current if
                if (tempoffset == 0 && FindWord(cmd[j], endpoint[0]))
                {
                    //if the first if is found store it's index in temp
                    tempoffset = j + 1;
                    ifreturn = false;

                }
                else if (tempoffset != 0 && count == 0 && FindWord(cmd[j], endpoint[1]))
                {

                    arranged.Add(--tempoffset, j);
                    Console.WriteLine("Temp should be stored with if offset:" + tempoffset + "and endif offset " + j);
                    j = tempoffset;
                    tempoffset = 0;
                    count = 0;
                    ifreturn = true;

                }
                //if if is already found that means temoffset and findif should be true for condition below
                else if (tempoffset != 0)
                {
                    //if again another if found increase the number else decrease it

                    if (FindWord(cmd[j], endpoint[0])) { count++; Console.WriteLine("Count increased " + count); }
                    else if (FindWord(cmd[j], endpoint[1])) { count--; Console.WriteLine("Count increased " + count); }
                }

            }
            if (ifreturn)
                return arranged;
            else
                return null;
        }

        /*
         * IF WHILE PARSING STARTS
         */

        /*
         * METHOD PARSING STARTS
         */

        /// <summary>
        /// Check the line for method declaration and store the method in method collection.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        public bool ParseMethod(string line, Methods method)
        {
            
            string[] cmd = line.Split(' ');
            if(cmd.Length == 2)
            {
                string[] methodandparam = cmd[1].Split('(');
                if(methodandparam.Length == 2)
                {
                    method.methodName = methodandparam[0];
                    string[] parameters = methodandparam[1].Split(',');
                    parameters[parameters.Length - 1] = parameters[parameters.Length - 1].Replace(")", "");
                    method.parameters= parameters;
                    return true;
                }

            }
            return false;
        }

        /// <summary>
        /// Check if the line contains the name of the method already stored.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool CheckForMethod(string line)
        {
            iterator = methods.CreateIterator();

            try
            {
                for (Methods method = iterator.First();
                !iterator.IsDone; method = iterator.Next())
                {
                    if (FindWord(line, method.methodName)) return true;
                }
            }
            catch (ArgumentOutOfRangeException) { }

            return false;
        }

        /// <summary>
        /// Parse the parameter of the 
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public string[] ParseMethodParameter(string cmd)
        {
            Console.WriteLine("****Inside ParseMethodParameter****");
            iterator = methods.CreateIterator();
            string[] methodandparam = cmd.Split('(');
            if (methodandparam.Length == 2)
            {
                Console.WriteLine(methodandparam[1]+ " trying to split this with , ");
                string[] parameters = methodandparam[1].Split(',');
                //Console.WriteLine(parameters.Length + "**"+((Methods)methods[0]).parameters.Length + "**" + ((Methods)methods[0]).methodName);

                if (parameters.Length>0)
                {
                    parameters[parameters.Length - 1] = parameters[parameters.Length - 1].Replace(")", "");


                    try
                    {
                        for (Methods method = iterator.First();
                        !iterator.IsDone; method = iterator.Next())
                        {
                            Console.WriteLine(method.methodName + "***" + methodandparam[0]);
                            if (method.methodName.Equals(methodandparam[0]) && method.parameters.Length == parameters.Length)
                            {
                                for (int i = 0; i < method.parameters.Length; i++)
                                {
                                    if (!method.parameters[i].Equals(""))
                                        variables.Add(method.parameters[i], parameters[i]);
                                }
                                return (string[])method.content.ToArray(typeof(string));
                            }
                            else throw new ArgumentException();
                        }
                    }
                    catch (ArgumentOutOfRangeException) { }

                    

                }
                
            }
            return new string[0];
        }

        public void RemoveVar(string cmd)
        {
            iterator = methods.CreateIterator();
            string[] methodandparam = cmd.Split('(');
            if (methodandparam.Length == 2)
            {
                string[] parameters = methodandparam[1].Split(',');
                if (parameters.Length > 0)
                {
                    parameters[parameters.Length - 1] = parameters[parameters.Length - 1].Replace(")", "");

                    try
                    {
                        for (Methods method = iterator.First();
                        !iterator.IsDone; method = iterator.Next())
                        {
                            if (method.methodName.Equals(methodandparam[0]) && method.parameters.Length == parameters.Length)
                            {
                                for (int i = 0; i < method.parameters.Length; i++)
                                {
                                    variables.Remove(method.parameters[i]);

                                }

                            }
                        }
                    }
                    catch (ArgumentOutOfRangeException) { }
                    
                }

            }
            
        }
        /*
         * METHOD PARSING ENDS
         */


    }
}
