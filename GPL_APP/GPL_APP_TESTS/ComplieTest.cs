﻿using System;
using GPL_APP;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPL_APP_TESTS
{
    [TestClass]
    public class ComplieTest
    {
        
        [TestMethod]
        public void LineSplitTest()
        {

            Compile compile = new Compile();
            string[] lineparse = compile.LineParser("moveto 150,150");

            string expected1 = "moveto";
            string actual1 = lineparse[0];

            string expected2 = "150,150";
            string actual2 = lineparse[1];

            Assert.AreEqual(expected1, actual1, "String wasn't not properly splited by LineParse");
            Assert.AreEqual(expected2, actual2, "String wasn't not properly splited by LineParse");
        }
    }
}
