﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineApp
{
    /// <summary>
    /// Used Abstract Factory design pattern
    /// </summary>
    public abstract class AbstractFactory
    {
        /// <summary>
        /// created abstract class getShape
        /// </summary>
        /// <param name="shapeType">type os string</param>
        public abstract Shape getShape(String shapeType);
    }
}
