﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineApp
{
    class Triangle : Shape
    {
        //variable declaration
        PointF[] points;
        
        //default constructor
        /// <summary>
        /// deefault triangle constructor
        /// </summary>
        public Triangle()
        {

        }
        
        //parameterized constructor
        /// <summary>
        /// Parameterized constructor of triangle for setting points parameter
        /// </summary>
        /// <param name="points"></param>
        public Triangle(PointF[] points)
        {
            this.points = points;
        }
        
        /// <summary>
        /// Parameterized constructor which extends the base class shape
        /// </summary>
        /// <param name="color"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Triangle(Color color, int x, int y) : base(color, x, y)
        {

        }
       
        //setter method of points
        /// <summary>
        /// Setter method for points
        /// </summary>
        /// <param name="points"></param>
        public void setPoints(PointF[] points)
        {
            this.points = points;
        }
       
        //getter of points
        /// <summary>
        /// Getter method for points
        /// </summary>
        /// <returns></returns>
        public PointF[] getPoints()
        {
            return this.points;
        }

        //shape fill method
        // shape fill method
        /// <summary>
        /// Inherited method of base class for filling the triangle shape
        /// </summary>
        /// <param name="g">c# Graphics function</param>
        /// <param name="c">c - c# Color function for brush color</param>
        public override void fill(Graphics g, Color c)
        {
            SolidBrush fill = new SolidBrush(c);
            g.FillPolygon(fill, points);
        }

        //shape draw method
        /// <summary>
        /// Inherited method of shape class which uses drawing library to draw a triangle
        /// </summary>
        /// <param name="g">c# Graphics function</param>
        /// <param name="c">c - c# Color function for pen color</param>
        /// <param name="thickness">integer thickness of a pen</param>
        public override void draw(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c);
            g.DrawPolygon(p, points);
        }
    }
}
