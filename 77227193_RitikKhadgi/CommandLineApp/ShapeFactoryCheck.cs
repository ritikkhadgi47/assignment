﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineApp
{
    /// <summary>
    /// ShapeFactory class unit testing
    /// </summary>
    public class ShapeFactoryCheck
    {
        /// <summary>
        /// checks if the shape is circle
        /// </summary>
        /// <param name="shape">string shape- type of shape</param>
        /// <returns>boolean- returns true if the shape is circle, false if not</returns>
        public bool isCircle(string shape)
        {
            if(shape == "circle")
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// checks if the shape is rectangle
        /// </summary>
        /// <param name="shape">string shape- type of shape</param>
        /// <returns>boolean- returns true if the shape is rectangle, false if not</returns>
        public bool isRectangle(String shape)
        {
            if(shape == "rectangle")
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// checks if the shape is triangle
        /// </summary>
        /// <param name="shape">string shape- type of shape</param>
        /// <returns>boolean- returns true if the shape is triangle, false if not</returns>
        public bool isTriangle(string shape)
        {
            if(shape == "triangle")
            {
                return true;
            }
            return false;
        }
    }
}
