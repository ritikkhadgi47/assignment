﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineApp
{
    /// <summary>
    /// CIrcle class extends Shape class
    /// draw the circle shape
    /// </summary>
    class Circle : Shape
    {

        //radius of a circle - integer
        int radius;
        /// <summary>
        /// default constructor
        /// </summary>
        public Circle() : base()
        {

        }

        //parameterized constructor
        /// <summary>
        /// Parameterized Circle constructor which takes the parameter color, x,y and radius
        /// </summary>
        /// <param name="color">c - c# Color function for shape color</param>
        /// <param name="x">cursor location at x-axis</param>
        /// <param name="y"> cursor location at y-axix</param>
        /// <param name="radius"> radius of a circle</param>
        public Circle(Color  color, int x, int y, int radius) : base(color, x, y)
        {
            this.radius = radius;

        }


        /// <summary>
        /// Inherited method of shape class which uses drawing library to draw a circle
        /// </summary>
        /// <param name="g">c# Graphics funtion</param>
        /// <param name="c">c - c# Color function for pen color</param>
        /// <param name="thickness">thickness of a pen</param>
        public override void draw(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            g.DrawEllipse(p, x, y, radius, radius);
        }

        //shape fill method
        /// <summary>
        /// Inherited method of base class for filling the circle shape
        /// </summary>
        /// <param name="g">c# Graphics function</param>
        /// <param name="c">c - c# Color function of brush color</param>
        public override void fill(Graphics g, Color c)
        {
            SolidBrush b = new SolidBrush(c);
            g.FillEllipse(b, x, y, radius, radius);
        }
        /// <summary>
        /// Setter method of a radius of a circle
        /// </summary>
        /// <param name="radius">integer radius of a circle</param>
        public void setRadius(int radius)
        {
            this.radius = radius;
        }
        /// <summary>
        /// Getter method of a radius of a circle
        /// </summary>
        /// <returns>integer radius of a circle</returns>
        public int getRadius()
        {
            return radius;
        }

        // set parameters of the shape
        /// <summary>
        /// Setter method for all values passed in the cmd
        /// </summary>
        /// <param name="color">c - c# Color function for shape color</param>
        /// <param name="list">c# list of all variables passed by user</param>
        public override void set(Color color, params int[] list)
        {
            base.set(color, list[0], list[1]);
            this.radius = list[2];
        }
    }
}
