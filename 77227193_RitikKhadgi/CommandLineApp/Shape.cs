﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineApp
{
    public abstract class Shape
    {
        //variable declaration
        protected Color color;
        protected int x, y;

        /// <summary>
        /// Default constructor to set default values to declared variable
        /// </summary>
        public Shape()
        {
            color = Color.Black;
            x = y = 0;
        }


        //parameterized constructor
        /// <summary>
        /// Paremeterized constructor to set values to declared variables
        /// </summary>
        /// <param name="color">c - c# Color function</param>
        /// <param name="x">integer x - position of a pen at x-axis</param>
        /// <param name="y">integer y - position of a pen at y-axis</param>
        public Shape(Color color, int x, int y)
        {
            this.color = color;
            this.x = x;
            this.y = y;
        }
        /// <summary>
        /// sets position of pen at x-axis
        /// </summary>
        /// <param name="x">integer x</param>
        public void setX(int x)
        {
            this.x = x;
        }
        /// <summary>
        /// gets position of a pen at y-axis
        /// </summary>
        /// <returns>integer x</returns>
        public int getX()
        {
            return x;
        }

        /// <summary>
        /// sets position of a pen at x-axis
        /// </summary>
        /// <param name="y">integer y</param>
        public void setY(int y)
        {
            this.y = y;
        }
        /// <summary>
        /// gets position of a pen at x-axis
        /// </summary>
        /// <param name="y">integer y</param>
        public int getY()
        {
            return y;
        }

        // set color and position of the cursor
        /// <summary>
        /// setter method for all values passed from cmd
        /// </summary>
        /// <param name="color">c - c# Color function</param>
        /// <param name="list">c# list</param>
        public virtual void set (Color color, params int[] list)
        {
            this.color = color;
            this.x = list[0];
            this.y = list[1];
        }


        // shape draw method
        /// <summary>
        /// Draw method which draw a shape using Graphics and Pen c# function
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c">c - c# Color function</param>
        /// <param name="thickness">integer thickness of a pen</param>
        public abstract void draw(Graphics g, Color c, int thickness);

        //shape fill method
        // shape fill method
        /// <summary>
        /// Fill method to fill the shape with color using SolidBrush and Graphics c# function
        /// </summary>
        /// <param name="g">c# Graphics function</param>
        /// <param name="c">c - c# Color function</param>
        public abstract void fill(Graphics g, Color c);
    }
}
