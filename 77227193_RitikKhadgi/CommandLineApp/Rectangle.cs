﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineApp
{
    class Rectangle : Shape
    {
        //variable declaration length and breadth of a rectangle
        int breadth, length;

        /// <summary>
        /// default constructor
        /// </summary>
        public Rectangle() : base()
        {

        }
        //parameterized constructor
        /// <summary>
        /// Prameterized constructor which extends base class shape
        /// </summary>
        /// <param name="color">c - c# Color function for pen color</param>
        /// <param name="x">integer x- position of a pen at x-axix</param>
        /// <param name="y">integer y - position of a pen at y-axis</param>
        /// <param name="breadth">integer breadth - breadth of a rectangle</param>
        /// <param name="length">integer length - length of a rectangle</param>
        public Rectangle(Color color, int x, int y, int breadth, int length) : base(color, x, y)
        {
            this.breadth = breadth;
            this.length = length;
        }

        /// <summary>
        /// Getter method of length
        /// </summary>
        /// <returns>integer length of a rectangle</returns>
        public int getLength()
        {
            return length;
        }
        /// <summary>
        /// Setter method of a length
        /// </summary>
        /// <param name="length">integer length of a rectangle</param>
        public void setLength(int length)
        {
            this.length = length;
        }
        /// <summary>
        /// Getter Method of breadth
        /// </summary>
        /// <returns>integer breadth of a rectangle</returns>
        public int getBreadth()
        {
            return breadth;
        }
        /// <summary>
        /// setter method for breadth of a rectangle
        /// </summary>
        /// <param name="breadth">integer breadth of a rectangle</param>
        public void setBreadth(int breadth)
        {
            this.breadth = breadth;
        }

        // shape draw method
        /// <summary>
        /// Inherited method of shape class which uses drawing library to draw a rectangle
        /// </summary>
        /// <param name="g">c# Graphics function</param>
        /// <param name="c">c - c# Color function for pen color</param>
        /// <param name="thickness">integer thickness of a pen</param>
        public override void draw(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, x, y, length, breadth);
        }

        // shape fill method
        /// <summary>
        /// Inherited method of base class for filling the rectangle shape
        /// </summary>
        /// <param name="g">c# Graphics function</param>
        /// <param name="c">c - c# Color function for brush color</param>
        public override void fill(Graphics g, Color c)
        {
            SolidBrush b = new SolidBrush(c);
            g.FillRectangle(b, x, y, length, breadth);
        }

        // set the parameters of the shape
        /// <summary>
        /// Setter method for all values passed in the cmd
        /// </summary>
        /// <param name="color">c - c# Color function for color of a shape</param>
        /// <param name="list">c# list of all variables passed by user</param>
        public override void set(Color color, params int[] list)
        {
            base.set(color, list[0], list[1]);
            this.length = list[2];
            this.breadth = list[3];
        }
    }
}
