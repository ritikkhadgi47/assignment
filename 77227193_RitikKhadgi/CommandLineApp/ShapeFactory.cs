﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineApp
{
    /// <summary>
    /// Used factory design pattern
    /// ShapeFactory extends the AbstractFactory(abstract factory design pattern) class
    /// </summary>
    public class ShapeFactory : AbstractFactory
    {
        /// <summary>
        /// abstract class to get the type of shape
        /// </summary>
        /// <param name="shapeType">string shapeType to filter the shape </param>
        /// <returns>shape object or null if not found</returns>
        public override Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToLower().Trim();  //to make entered shape type case insensitive
            if (shapeType == null)
            {
                return null;
            }

            if (shapeType.Equals("circle"))
            {
                return new Circle();

            }
            else if (shapeType.Equals("rectangle"))
            {
                return new Rectangle();
            }
            else if (shapeType.Equals("triangle"))
            {
                return new Triangle();
            }
            else
            {
                //throw an exception when shapetype is does not exist
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }
        }
    }
}
