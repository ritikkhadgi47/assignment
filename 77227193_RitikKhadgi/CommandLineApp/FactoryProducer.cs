﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineApp
{
    /// <summary>
    /// class to determine which factory object to create
    /// </summary>
    class FactoryProducer
    {
        //create factory of different color and shape
        /// <summary>
        /// creates factory object based on user choice
        /// </summary>
        /// <param name="choice">string-type of object to create</param>
        /// <returns>factory object or null based on choice</returns>
        public static AbstractFactory getFactory(String choice)
        {
            //check if choice is shape or color
            if (choice.Equals("Shape"))
            {
                return new ShapeFactory();
            }
            return null;
        }
    }
}
