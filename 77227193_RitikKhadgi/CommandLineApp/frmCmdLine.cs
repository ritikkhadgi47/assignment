﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandLineApp
{
    public partial class frmCmdLine : Form
    {
        //check either to make object or not
        Boolean fill, paintTriangle;
        //hold the command entered in the array
        String[] words;
        //cursor x and y position
        int moveX, moveY;
        //thickness of a pen
        int thickness;
        //cmd typed in single command line textbox
        String actionCmd;
        //store cmd typed in command textbox
        String cmds;
        ArrayList shapes = new ArrayList();
        List<Triangle> triangleObjects;
        List<Variable> variableObjects;
        //PointF[] points;

        Color c;
        Shape s, s1;
        Variable variable;

        int counter, loopCounter;

        //save the cmd in cmd textbox in the chosen directory
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
        /// <summary>
        /// default constructor to initialize components;
        /// </summary>
        public frmCmdLine()
        {
            InitializeComponent();
            AbstractFactory abstractFactory = FactoryProducer.getFactory("Shape");

            try
            {
                shapes.Add(abstractFactory.getShape("Circle"));
                shapes.Add(abstractFactory.getShape("Rectangle"));
                //shapes.Add(abstractFactory.getShape("Triangle"));
                s1 = abstractFactory.getShape("triangle");

            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Invalid shape: " + e);

            }
        }

        //open the saved cmd
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void frmCmdLine_Load(object sender, EventArgs e)
        {
            triangleObjects = new List<Triangle>();
            variableObjects = new List<Variable>();
            c = Color.Black;
        }

        private void txt_cmd_TextChanged(object sender, EventArgs e)
        {
            actionCmd = txt_cmd.Text.ToLower(); //change the type cmd to lowercase making it case insensitive
        }

        private void btn_execute_Click(object sender, EventArgs e)
        {
            switch (actionCmd)
            {
                case "run":
                    try
                    {
                        AbstractFactory abstractFactory = FactoryProducer.getFactory("Shape");
                        cmds = txt_cmdLineCode.Text.ToLower();
                        char[] delimiters = new char[] { '\r', '\n' };//remove new line entries
                        string[] cmd_array = cmds.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                        
                        for (int i = 0;  i< cmd_array.Length; i++)
                        {
                            //single cmd line
                            String cmd_line = cmd_array[i];
                            //splits single line cmd when spaced and stores in an array
                            char[] value_cmd = new char[] { ' ' };
                            //holds splitted code line
                            words = cmd_line.Split(value_cmd, StringSplitOptions.RemoveEmptyEntries);
                            if (Regex.IsMatch(words[0], @"^[a-zA-Z]+$") && words[1].Equals("+"))
                            {
                                variableObjects[variableObjects.FindIndex(x => x.variable.Contains(words[0]))].
                                    setValue(variableObjects[variableObjects.FindIndex(x => x.variable.Contains(words[0]))]
                                    .getValue() + Convert.ToInt32(words[2]));
                            }
                            if ((Regex.IsMatch(words[0], @"^[a-zA-Z]+$") && words[1].Equals("=")))
                            {
                                if (variableObjects.Exists(x => x.variable == words[0] && x.value == Convert.ToInt32(words[2])) == true)
                                {
                                    Console.WriteLine("variable exists: ");

                                }
                                //add new variable if variableObject list is empty
                                else if (variableObjects == null || variableObjects.Count == 0)
                                {
                                    variable = new Variable();
                                    variable.setVariable(words[0]);
                                    variable.setValue(Convert.ToInt32(words[2]));
                                    variableObjects.Add(variable);
                                }
                                else
                                {
                                    //checks if variable exists or not
                                    if (!variableObjects.Exists(x => x.variable == words[0]))
                                    {
                                        variable = new Variable();
                                        variable.setVariable(words[0]);
                                        variable.setValue(Convert.ToInt32(words[2]));
                                        variableObjects.Add(variable);
                                    }
                                    //add new variable value to variableObjects if does not exists
                                    else
                                    {
                                        variable = new Variable();
                                        variable.setVariable(words[0]);
                                        variable.setValue(Convert.ToInt32(words[2]));
                                        variableObjects[variableObjects.FindIndex(x => x.variable.Contains(words[0]))] = variable;
                                    }
                                }
                            }


                            //checks if words has 'move' command then
                            if (words[0] == "moveto")
                            {
                                moveX = Convert.ToInt32(words[1]);//sets the location the shape xaxis
                                moveY = Convert.ToInt32(words[2]);//sets the location of the shape yaxis
                            }
                            //change the color of the pen
                            if (words[0] == "pen")
                            {
                                if(!(words.Length == 3))
                                {
                                    MessageBox.Show("!!! Invalid Command !!!\ncmd should be 'pen red 2'");
                                }
                                else
                                {
                                    thickness = Convert.ToInt32(words[2]); //typecasting string to int

                                    if (words[1] == "red")
                                    {
                                        c = Color.Red; //change color to red
                                    }
                                    else if (words[1] == "yellow")
                                    {
                                        c = Color.Yellow; // change color to yellow
                                    }
                                    else if (words[1] == "blue")
                                    {
                                        c = Color.Blue; //change color to blue
                                    }
                                    else if (words[1] == "green")
                                    {
                                        c = Color.Green; //change color to green
                                    }
                                    else
                                    { 
                                        c = Color.Black; //change color to black
                                    }
                                }                               
                            }
                            //drawing the shape
                            if (words[0] == "drawto")
                            {
                                //check if the shape is circle
                                if(words[1] == "circle")
                                {
                                    counter += 1;
                                    //check if the given cmd is of correct
                                    if(!(words.Length == 3))
                                    {
                                        MessageBox.Show("!!! Invalid Command !!!\ncmd should be 'drawTo circle 100'");
                                    }
                                    else
                                    {
                                        if (variableObjects.Exists(x => x.variable == words[2]) == true)
                                        //assigns variable value to paint code parameter value
                                        {
                                            words[2] = Convert.ToString(variableObjects.ElementAt(variableObjects.
                                                FindIndex(x => x.variable.Contains(words[2]))).getValue()); //variable value to radius parameter
                                        }
                                        else
                                        {
                                            MessageBox.Show("!!!Variable "+ words[2] + " does not exist.!!!");
                                        }

                                        for (int k = shapes.Count - 1; k >= 0; k--)
                                        {
                                            if (shapes[k] is Circle)
                                            {
                                                Circle circleObj = (Circle)shapes[k];
                                                if (circleObj.getX() == moveX && circleObj.getY() == moveY && circleObj.getRadius() == Convert.ToInt32(words[2]))
                                                {
                                                    if (circleObj.getRadius() == Convert.ToInt32(words[2]))
                                                    {
                                                        //check if given details exist
                                                        MessageBox.Show("!!!Circle object exists with given parameters");
                                                        break;
                                                    }

                                                }
                                                else
                                                {
                                                    //getting circle object
                                                    s = abstractFactory.getShape("circle");
                                                    //setting the values in circle object
                                                    s.set(c, moveX, moveY, Convert.ToInt32(words[2]));
                                                    //adding circle shape into shapes list
                                                    shapes.Add(s);
                                                }
                                            }
                                        }
                                    }   
                                }
                                //checking if the shape is rectangle
                                if (words[1] == "rectangle")
                                {
                                    counter += 1;
                                    //check if the given cmd had lenght of 4
                                    if(!(words.Length == 4))
                                    {
                                        MessageBox.Show("!!! Invalid Command !!!\ncmd should be 'drawTo rectangle 100 200'");
                                    }
                                    else
                                    {
                                        if (variableObjects.Exists(x => x.variable == words[2] == true))
                                        {
                                            words[2] = Convert.ToString(variableObjects.ElementAt(variableObjects.
                                                FindIndex(x => x.variable.Contains(words[2]))).getValue()); //variable value to height parameter
                                        }
                                        else
                                        {
                                            MessageBox.Show("!!!Variable " + words[2] + " does not exist.!!!");
                                        }
                                        if (variableObjects.Exists(x => x.variable == words[3]) == true)
                                        {
                                            words[3] = Convert.ToString(variableObjects.ElementAt(variableObjects.
                                                FindIndex(x => x.variable.Contains(words[3]))).getValue()); //variable value to width parameter
                                        }
                                        else
                                        {
                                            MessageBox.Show("!!!Variable " + words[3] + " does not exist.!!!");
                                        }

                                        for (int k = shapes.Count - 1; k >= 0; k--)
                                        {
                                            if (shapes[k] is Rectangle)
                                            {
                                                Rectangle rectObj = (Rectangle)shapes[k];
                                                if (rectObj.getX() == moveX && rectObj.getY() == moveY && rectObj.getLength() == Convert.ToInt32(words[2]) && rectObj.getBreadth() == Convert.ToInt32(words[3]))
                                                {
                                                    //check if given rectangle already existts
                                                    if(rectObj.getLength() == Convert.ToInt32(words[2]) || rectObj.getBreadth() == Convert.ToInt32(words[3]))
                                                    {
                                                        MessageBox.Show("!!!Rectangnle with given parameter exists!!!");
                                                    }
                                                }
                                                else
                                                {
                                                    //creating rectalge object
                                                    s = abstractFactory.getShape("rectangle");
                                                    //setting all the values to rectangle object
                                                    s.set(c, moveX, moveY, Convert.ToInt32(words[2]), Convert.ToInt32(words[3]));
                                                    //adding rectangle object to shapes list
                                                    shapes.Add(s);
                                                }
                                            }
                                        }                                     
                                    }
                                }
                                //checking if the shape if triangle
                                if(words[1] == "triangle")
                                {
                                    counter += 1;
                                    if(!(words.Length == 2))
                                    {
                                        MessageBox.Show("!!! Invalid Command !!!\ncmd should be 'draw triangle'");
                                    }
                                    else
                                    {
                                        Triangle triangle = new Triangle();
                                        PointF[] points = { new PointF(100, 100), new PointF(200, 100), new PointF(150, 10) };
                                        triangle.setPoints(points);
                                        triangleObjects.Add(triangle);
                                        paintTriangle = true;
                                    }                                    
                                }
                            }
                            if (words[0] == "if")
                            {
                                //getting declared variable
                                string v_name = words[1];
                                //getting the value of a variable
                                int value = Convert.ToInt32(words[3]);
                                if (variableObjects.Exists(x => x.variable == words[1]) == true
                                    && variableObjects.Exists(x => x.value == Convert.ToInt32(words[3])) == true)
                                //checks if condition defined in if condition matches with variable objects list
                                {
                                    Console.WriteLine("Entered into if statement");
                                }
                                else
                                {//directed to end if line
                                    i = Array.IndexOf(cmd_array, "end if");
                                }
                            }
                            //checks if the loop statement is written
                            if (words[0] == "loop") 
                            {
                                loopCounter = Convert.ToInt32(words[1]);
                            }

                            //checkes for 'end loop' statement
                            if (cmd_array[i] == "end loop")
                            {
                                //checks if the loopcounter is greater than draw counter
                                if (counter < loopCounter)
                                {
                                    i = Array.IndexOf(cmd_array, "loop " + loopCounter);
                                    Console.WriteLine(i);
                                }
                                else
                                {
                                    i = Array.IndexOf(cmd_array, "end loop");
                                }
                            }
                            //fill the shape
                            if (words[0] == "fill")
                            {
                                if(words[1] == "on")
                                {
                                    fill = true;
                                }
                                else if (words[1] == "off")
                                {
                                    fill = false;
                                }
                                else
                                {
                                    MessageBox.Show("!!! Invalid Command !!!\ncmd should be 'fill on/off'");
                                }
                            }
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        Console.WriteLine("Error: " + ex.Message + "\n");
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine("Error:" + " " + ex);

                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        Console.WriteLine("Error" + " " + ex);
                    }
                    pb_output.Refresh();
                    break;
                case "reset":   //move pen back to initial position
                    moveX = 0;
                    moveY = 0;
                    break;
                case "clear":   //move pen back to initial position and clear the program window
                    shapes.Clear();
                    moveX = 0;
                    moveY = 0;
                    fill = false;
                    triangleObjects.Clear();
                    txt_cmdLineCode.Clear();
                    pb_output.Refresh();
                    
                    break;
                default:
                    //if single cmd line is empty
                    MessageBox.Show("The command line box is empty\n" +
                       "Or\n" +
                       "Must be: 'run' to execute the command\n" +
                       "Must be: 'clear' to clear program window and picture box\n"+
                       "Must be: 'reset' to position cursor back to initial position");
                    break;
            }
        }

        private void pb_output_Paint(object sender, PaintEventArgs e)
        {
            //c# in-built graphics library
            Graphics g = e.Graphics;

            //check if the draw triangle is true
            if(paintTriangle == true)
            {
                foreach(Triangle triangleObject in triangleObjects)
                {
                    triangleObject.draw(g, c, thickness);
                    //check if fill is on or not
                    if (fill == true)
                    {
                        triangleObject.fill(g, c);
                    }
                }
            }

            //loop to draw shape objects if added to an arrayy
            for (int i = 0; i < shapes.Count; i++)
            {
                s = (Shape)shapes[i];
                if (s != null)
                {
                    s.draw(g, c, thickness);
                    //check if fill is one or not
                    if (fill == true)
                    {
                        s.fill(g, c);
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Shape in array");
                }
            }
        }

        //save the syntax in program window to the computer
        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, txt_cmdLineCode.Text);
            }
        }

        //open the command from computer file system
        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txt_cmdLineCode.Text = File.ReadAllText(openFileDialog.FileName);
            }
        }

        private void pb_output_Click(object sender, EventArgs e)
        {

        }

        private void txt_cmdLineCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //details about the syntax and parameters to be written in proram window
        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("!!!!BELOW ARE THE CORRECT SYNTAX AND PARAMETERS THAT IS TO BE USED IN THE PROGRAM WINDOW!!!!\n" +
                "*******************************\n" +
                "Hint\n" +
                "*******************************\n" +
                "TO DISPLAY SHAPES COMMANDS\n" +
                "-----------------------------\n" +
                "drawTo rectamgle 200 100\n" +
                "drawTo circle 100\n" +
                "drawTo triangle\n" +
                "-----------------------------\n" +
                "TO PAINT SHAPE WITH PARAMETER\n" +
                "rad = 100\n" +
                "draw circle rad\n" +
                "length = 100\n" +
                "breadth = 150\n"+
                "draw rectangle length breadth\n" +
                "-----------------------------\n" +
                "FOR LOOPING" +
                "loop 4\n" +
                "rad=rad+50\n"+
                "draw circle rad\n"+
                "end loop\n"+
                "-----------------------------\n" +
                "FOR IF STATEMENT:\n" +
                "a = 5\n" +
                "if a=5\n" +
                "draw circle 100\n"+
                "end if\n"+
                "-----------------------------\n" +
                "TO MOVE CURSOR TO ANOTHER POSITION\n" +
                "moveTo 100 150\n" +
                "------------------------------\n" +
                "TO CHANGE THE COLOR AND SIZE OF THE PEN\n" +
                "pen red 23\n"+
                "-----------------------------\n" +
                "TO ENABLE/DISABLE FILL OPTION"+
                "fill on/off\n"+
                "-----------------------------\n" +
                "\nver 1.1" +
                "\n Copyright: Ritik Khadgi");

        }

        

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
