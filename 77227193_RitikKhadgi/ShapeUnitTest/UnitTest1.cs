using CommandLineApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ShapeUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            
        }
        [TestMethod]
        public void CheckTriangleShape()
        {
            var shapeFactoryCheck = new ShapeFactoryCheck();
            bool result = shapeFactoryCheck.isTriangle("triangle");

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckShapeObject()
        {
            var factoryProducerCheck = new FactoryProducerCheck();

            bool result = factoryProducerCheck.isShape("shape");

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckCircleShape()
        {
            var shapeFactoryCheck = new ShapeFactoryCheck();
            bool result = shapeFactoryCheck.isCircle("circle");

            Assert.AreEqual(true, result);
        }
    }
}
